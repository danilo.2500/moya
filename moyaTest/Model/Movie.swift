//
//  Movie.swift
//  moyaTest
//
//  Created by Danilo Henrique on 27/03/18.
//  Copyright © 2018 Danilo Henrique. All rights reserved.
//

import Foundation

struct Movie:Decodable {
    let id:Int!
    let posterPath:String
    var videoPath:String?
    let backdrop:String
    let title: String
    var releaseDate:String
    var rating:Double
    let overview:String
    
    private enum CodingKeys: String,CodingKey{
        //no json algumas variaveis estao escritar numa nomenclatura diferente (poster_path em ves de posterPath), este codigo mostra qual corresponde a qual
        case id, posterPath = "poster_path",videoPath, backdrop = "backdrop_path", title, releaseDate = "release_date", rating = "vote_average",overview
    }

}

struct APIResults:Decodable {
    let page :Int
    let numResults:Int
    let numPages:Int
    let movies:[Movie]
    
    private enum CodingKeys:String,CodingKey{
        case page,numResults = "total_results", numPages = "total_pages", movies = "results"
    }
    
}
