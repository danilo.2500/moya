//
//  API.swift
//  moyaTest
//
//  Created by Danilo Henrique on 27/03/18.
//  Copyright © 2018 Danilo Henrique. All rights reserved.
//

import Foundation
import Moya

class API {
    static let apiKey = "1f54bd990f1cdfb230adb312546d765d"
    static let provider = MoyaProvider<MovieApi>(plugins: [NetworkLoggerPlugin(verbose: true)])

    static func getNewMovies(page:Int,completion:@escaping ([Movie])->() ){
        provider.request(.newMovies(page: page)) { (result) in
            
            
            switch result{
            case let .success(response):do {
                
                    let results = try JSONDecoder().decode(APIResults.self, from: response.data)
                        completion(results.movies)
            }catch let err{
                print(err)
                
            }
                
            case let .failure(error):
                print(error)
            }
        }
        
        
    }
}
