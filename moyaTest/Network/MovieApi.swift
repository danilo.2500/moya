//
//  MovieApi.swift
//  moyaTest
//
//  Created by Danilo Henrique on 27/03/18.
//  Copyright © 2018 Danilo Henrique. All rights reserved.
//

import Foundation
import Moya

enum MovieApi{
    //tipos de retorno de informaçoes da api
    //posso querer os melhores filmes ou os mais atualizados
    
    case reco(id:Int)
    case topRated(page:Int)
    case newMovies(page:Int)
    case video(id:Int)
}

extension MovieApi: TargetType {
    
    
    //url padrao da api
    var baseURL: URL {
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/") else { fatalError("baseURL could not be configured") }
        return url
    }
    
    //define o path de cada url, montando a ulr de acordo com o caso especifico a ser baixado
    // tipo, no reco, baixamos um filme em especifico, ou seja precisamos o final da url pra combinar com a url padrao da api
    var path: String {
        switch self {
        case .reco(let id):
            return "\(id)/recommendations"
        case .topRated:
            return "popular"
        case .newMovies:
            return "now_playing"
        case .video(let id):
            return "\(id)/videos"
        }
    }
    
    
    // define o tipo de metodo que vamos usar na url
    // pode ser um desses GET POST PUT
    var method: Moya.Method {
        switch self {
        case .reco, .topRated, .newMovies, .video:
            return .get
        }
    }
    
    
    //passa os parametros pra API
    //tipo, se o cara que baixar o topRated, essa variavel vai retornar um dicionario com page e apikey
    var parameters: [String : Any]? {
        switch self {
        case .reco, .video:
            return ["api_key": API.apiKey]
        case .topRated(let page), .newMovies(let page):
            return ["page": page, "api_key": API.apiKey]
        }
    }
    
    //this is where we define what type of encoding we want to use for our API. Parameters can either be passed using headers or query strings. The Movie Database API uses query strings so we return query strings.

    var parameterEncoding: ParameterEncoding {
        switch self {
        case .reco, .topRated, .newMovies, .video:
            return URLEncoding.queryString
        }
        
    }
    
    //this is where we define the type of http task each endPoint perform. Tasks can be of type request, upload or download.
    var task: Task {
        switch self {
        case .reco, .topRated, .newMovies, .video:
            return .requestPlain
        }
    }
    //we will cover this later in the series. But basically this is a method solely for testing our API responses and creating a Mock API. For now just return Data().
    var sampleData: Data {
        return Data()
    }
    
    //
    var headers: [String : String]? {
        return nil
    }
    
}
