//
//  ViewController.swift
//  moyaTest
//
//  Created by Danilo Henrique on 27/03/18.
//  Copyright © 2018 Danilo Henrique. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        API.getNewMovies(page: 1) { (movies) in
            print(movies)
        }
    }


}

